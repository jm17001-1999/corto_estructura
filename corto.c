#include <stdio.h>

int main() {
	int asientos=25;
	int sala[5][5];
	int opcion, fil, col;
	for(int i=0;i<5;i++){
		for(int a=0;a<5;a++){
			sala[i][a]=0;
		}
	}
	do{
		printf("1. Comprar entrada\n 2. Ver ganacias\n *cualquier otro numero para salir.\n");
		scanf("%d",&opcion);
		switch(opcion){
			case 1:
				printf("---------------VENTA DE BOLETOS---------------\n");
				printf("¿Qué posicion desea? Ingrese fila seguida de la columna\n");
				//pedimos la posicion
				scanf("%d%d",&fil,&col);
				//verificamos que el asiento no este ocupado.
				if(sala[fil][col]==0){
					printf("Gracias por su compra.\n");
					sala[fil][col]=1;
				} else if(sala[fil][col]==1){
					printf("Asiento ocupado.\n");
				} else {
					printf("Asiento no existe.\n");
				}
				break;
			case 2:
				printf("---------------GANANCIAS---------------\n");
			    float ganancias=0;	
				for (int a=0;a<5;a++){
					for (int i=0;i<5;i++){
						//verificamos en que fila estamos para asi ver las ganacias.
						if (a==0){
							if (sala[a][i]==1){
								ganancias=ganancias+5.0;
							}
						} else if (a==4) {
							if (sala[a][i]==1){
								ganancias=ganancias+2.5;
							}
						} else {
							if (sala[a][i]==1){
								ganancias=ganancias+3.5;
							}
						}
					}
				}
				printf("Ganancias: %f", ganancias);	
				printf("\n");
				break;
		}
	} while(opcion==1 || opcion==2);
	return 0;
}
